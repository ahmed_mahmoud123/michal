var express=require('express');
var app=express();
const fs = require("fs");
var path = require('path');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json({limit:'50mb'});
let  pool;
app.use(function (req, res, next) {
    //res.header('transfer-encoding', 'chunked');
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Headers', 'x-access-token,Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});
var sql = require('mssql')

async function connectDB(dbName,next,request) {
    try {
        //const pool =  await sql.connect('mssql://sa:dataset123@192.168.35.4:1433/EGYPTTest');
        pool = await sql.close();
        console.log('mssql://sa:dataset123@192.168.35.4:1433/'+dbName);
        pool =  await sql.connect('mssql://sa:dataset123@192.168.35.4:1433/'+dbName);
        request.pool = pool;
        console.log("Connected!!");
        next();
    } catch (err) {
        console.log(err);
    }
};

app.use(jsonMiddelware);
app.use(function(request,response,next){
  connectDB(request.body.dbName,next,request);
});


// Loading Routes
fs.readdirSync(path.join(__dirname,"routes")).forEach(file=>{
    let route=require(`./routes/${file}`);
    app.use(`/${route.route_name}`,route);
});

app.listen(4000,()=>{
  console.log("Listening....");
});
