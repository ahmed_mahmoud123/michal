var sql = require('mssql');

class Order{

  async orders(resp,sales,date,end_date){
    let order_date = this.convertStringToDate(date).toISOString().replace("T22", "T00");
    let beforeWeek = new Date(this.convertStringToDate(date).getTime() - (60*60*24*7*1000));
    let endDate = end_date != undefined ? end_date : order_date;
    let endDateValue = this.convertStringToDate(endDate).toISOString().replace("T22", "T00");

    if(order_date == endDateValue){
      order_date = beforeWeek.toISOString().replace("T22", "T00");
    }
    const result = await sql.query`select * from VIW_FQ_Wrk_Ord as o ,VIW_FQ_Customers as c where o.CustId = c.Accounts_CODE and AssignedTo=${sales} and status = 2  and CallOrigin <= ${endDateValue} and CallOrigin >= ${order_date}`;
    resp.json(result);
  }

  async changeOrderStatus(resp,id,status,note,date=undefined){
    let result ={};
    if(status == 4 ){
       //date= this.convertStringToDate(date).toISOString().replace("T22", "T00");
       let date_note = date +"  "+note;
       result = await sql.query`update VIW_FQ_Wrk_Ord set status = ${status}, note=${date_note} where Serial=${id}`;
    }else{
       await sql.query`update VIW_FQ_Wrk_Ord set note=${note} where Serial=${id}`;
       result = await sql.query`update VIW_FQ_Wrk_Ord set status = ${status}, note=${note} where Serial=${id}`;
    }
    resp.json(result);
  }

  convertStringToDate(date){
    var dateString = date;
    var dataSplit = dateString.split('/');
    var dateConverted;
    if (dataSplit[2].split(" ").length > 1) {
      var hora = dataSplit[2].split(" ")[1].split(':');
       dataSplit[2] = dataSplit[2].split(" ")[0];
       dateConverted = new Date(dataSplit[2], dataSplit[1], dataSplit[0]);
    } else {
      dateConverted = new Date(dataSplit[2], dataSplit[1], dataSplit[0]);
    }
    dateConverted.setHours(0,0,0);
    return dateConverted;
  }

}

module.exports=new Order();
