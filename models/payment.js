var sql = require('mssql');

class Payment{

  async getCheq(resp,code){
    const result = await sql.query`select top 100 * from ViwUnpChq where ChqOrg=${code} and chqstat in('0','6') order by DDATE`;
    resp.json(result);
  }

  async insertCheq(resp,cheque){
    let  result = await sql.query` insert into FQCheckEx(serial,CHEQUE,cheqser,VDATE,amt,vchr,type,Recuser,WrkOrdNum,RATE,CURR,PERIOD,CASHACCOUNT) values(123,${cheque.CHEQUE},${cheque.CHQSER},${cheque.VDATE},${cheque.AMT},1234,${cheque.TYPE},${cheque.salsMan},${cheque.WrkOrdNum},${cheque.RATE},${cheque.CURR},${cheque.Period},${cheque.cashaccount})`;
    result = await sql.query`update VIW_FQ_Wrk_Ord set status = ${cheque.status}, note=${cheque.date_note} where Serial=${cheque.id}`;
    resp.json(result);
  }


  async insertReciept(resp,cheque){
    let  result = await sql.query`insert into FqReceipt(TYPE,VCHR,Period,VDATE,CURR,RATE,Gref,Bok,CODE,CRDB,LINNUM,BAMT,amt,recuser,slsman,pay,payn,CashAccount,remarks,WrkOrdNum ) values('J',0,'2007',${new Date()},0,0,0,0,0,0,0,${cheque.amount},${cheque.amount},${cheque.salsMan},${cheque.salsMan},${cheque.pay},${cheque.payn},${cheque.cashaccount},${cheque.remarks},${cheque.WrkOrdNum})`;
    //console.log(`insert into FqReceipt(TYPE,VCHR,Period,VDATE,CURR,RATE,Gref,Bok,CODE,CRDB,LINNUM,BAMT,amt,recuser,slsman) values('J',0,'2007',${new Date()},0,0,0,0,0,0,0,${cheque.amount},${cheque.amount},${cheque.salsMan},${cheque.salsMan})`);
    result = await sql.query`update VIW_FQ_Wrk_Ord set status = ${cheque.status}, note=${cheque.note} where Serial=${cheque.id}`;
    resp.json(result);
  }


  async getSum(resp,salesman){
    let cheqSum = 0;
    let recieptSum = 0;
    let cheqResult = await sql.query`select sum(amt) as recieptSum from FqReceipt where slsman=${salesman}`;
    console.log(JSON.stringify(cheqResult));
    cheqResult = cheqResult.recordset.pop();
    if(cheqResult){
      cheqSum = Number(cheqResult["recieptSum"]);
    }
    let receiptResult = await sql.query`select sum(amt) as cheqSum from FQCheckEx where Recuser=${salesman}`;
    console.log(JSON.stringify(receiptResult));
    receiptResult = receiptResult.recordset.pop();
    if(receiptResult){
      recieptSum = Number(receiptResult["cheqSum"]);
    }
    console.log(recieptSum + cheqSum);
    resp.json({
      sum:recieptSum + cheqSum
    });
  }


  async closeReciept(resp,cheque){
    console.log(cheque);
    let result = await sql.query`update VIW_FQ_Wrk_Ord set status = ${cheque.status}, note=${cheque.note} where Serial=${cheque.id}`;
    console.log(result);
    resp.json(result);
  }
}

module.exports=new Payment();
