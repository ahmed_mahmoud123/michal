var sql = require('mssql');
var fs = require('fs');

class Customer{

  async get(resp,id){
    const result = await sql.query`select * from VIW_FQ_Customers where Accounts_CODE=${id}`;
    resp.json(result);
  }

  async updateLocation(resp,id,long,lat){
    const result = await sql.query`update VIW_FQ_Customers set GPS2=${lat} , GPS1=${long} where Accounts_CODE=${id}`;
    //const result = await sql.query`select * from VIW_FQ_Customers where Accounts_CODE=${id}`;
    resp.json(result);
  }

  async saveDetails(resp,req,customer){
    let img1 = this.decodeBase64Image(customer.img1);
    fs.writeFileSync(__dirname+'/../temp1.png', customer.img1.replace(/^data:image\/png;base64,/, ""),'base64');
    let fileData1 = fs.readFileSync(__dirname+'/../temp1.png', 'binary');

    fs.writeFileSync(__dirname+'/../temp2.png', customer.img2.replace(/^data:image\/png;base64,/, ""),'base64');
    let fileData2 = fs.readFileSync(__dirname+'/../temp2.png', 'binary');

    fs.writeFileSync(__dirname+'/../temp3.png', customer.img3.replace(/^data:image\/png;base64,/, ""),'base64');
    let fileData3 = fs.readFileSync(__dirname+'/../temp3.png', 'binary');

    fs.writeFileSync(__dirname+'/../temp4.png', customer.img4.replace(/^data:image\/png;base64,/, ""),'base64');
    let fileData4 = fs.readFileSync(__dirname+'/../temp4.png', 'binary');

    var binBuff1 = new Buffer(fileData1, 'binary');
    var binBuff2 = new Buffer(fileData2, 'binary');
    var binBuff3 = new Buffer(fileData3, 'binary');
    var binBuff4 = new Buffer(fileData4, 'binary');

    var ps = new sql.PreparedStatement(req.pool);
    ps.input('img1', sql.VarBinary);
    ps.input('img2', sql.VarBinary);
    ps.input('img3', sql.VarBinary);
    ps.input('img4', sql.VarBinary);

    ps.prepare(`insert into FQProspectCust(name,img1,img2,img3,img4,Code,Tel1,Tel2,Tel3,Address,Type,GPS1,GPS2,salesman) values('${customer.name}',@img1,@img2,@img3,@img4,'123','${customer.Tel1}','${customer.Tel2}','${customer.Tel3}','${customer.Address}','12',${customer.location.lat},${customer.location.lng},${customer.salesman})`, function (err) {
        // check err
        ps.execute({img1: binBuff1,img2: binBuff2,img3: binBuff3,img4: binBuff4}, function(err, records) {
            // check err
            console.log(records);
            console.log(err);
            ps.unprepare(function(err) {
                // check err
                // If no error, it's been inserted!
                console.log(err);
                resp.json(err);
            });
        });
    });

    //const result = await sql.query`insert into FQProspectCust(name,Tel1,Tel2,Tel3,img1) values(${customer.name},${customer.Tel1},${customer.Tel2},${customer.Tel3},${img1})`;
    //resp.json(result);

  }

  async decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};
      if (matches.length !== 3) {
        return new Error('Invalid input string');
      }
      response.type = matches[1];
      response.data = new Buffer(matches[2], 'base64');
      return response;
  }

}

module.exports=new Customer();
