var express=require('express');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json();
let orderModel=require('../models/order');

let router=express.Router();

router.post('/',jsonMiddelware,(req,resp)=>{
  orderModel.orders(resp,req.body.sales,req.body.vdate,req.body.end_date);
});

router.post('/update',jsonMiddelware,(req,resp)=>{
  orderModel.changeOrderStatus(resp,req.body.id,req.body.status,req.body.note,req.body.date);
});

router.route_name="orders";
module.exports=router;
