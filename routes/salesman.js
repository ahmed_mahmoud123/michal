var express=require('express');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json();
let salesmanModel=require('../models/salesman');

let router=express.Router();

router.post('/login',jsonMiddelware,(req,resp)=>{
  salesmanModel.login(resp,req.body.username,req.body.password);
});

router.route_name="salesman";
module.exports=router;
