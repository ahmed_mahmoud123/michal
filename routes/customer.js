var express=require('express');
let customerModel=require('../models/customer');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json({limit:'50mb'});
let router=express.Router();


router.post('/saveDetails',jsonMiddelware,(req,resp)=>{
  customerModel.saveDetails(resp,req,req.body);
});


router.get('/:id',(req,resp)=>{
  customerModel.get(resp,req.params.id);
});



router.post('/:id/:lat/:long',(req,resp)=>{
  customerModel.updateLocation(resp,req.params.id,req.params.lat,req.params.long);
});
router.route_name="customer";
module.exports=router;
