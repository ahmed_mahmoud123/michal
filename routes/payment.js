var express=require('express');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json();
let paymentModel=require('../models/payment');

let router=express.Router();

router.post('/getCheq',jsonMiddelware,(req,resp)=>{
  paymentModel.getCheq(resp,req.body.code);
});

router.post('/insertCheq',jsonMiddelware,(req,resp)=>{
  paymentModel.insertCheq(resp,req.body.cheque);
});

router.post('/getSum',jsonMiddelware,(req,resp)=>{
  paymentModel.getSum(resp,req.body.salesman);
});

router.post('/insertReciept',jsonMiddelware,(req,resp)=>{
  paymentModel.insertReciept(resp,req.body.reciept);
});

router.post('/closeReciept',jsonMiddelware,(req,resp)=>{
  paymentModel.closeReciept(resp,req.body.reciept);
});


router.route_name="payment";
module.exports=router;
